const assert = require('assert');
const utils = require('tramway-core-testsuite');
const lib = require('../index.js');
var describeCoreClass = utils.describeCoreClass;
var describeFunction = utils.describeFunction;

describe("Simple acceptance tests to ensure library returns what's promised.", function(){
    describe("Should return a proper 'ReactRouterStrategy' class", describeCoreClass(
        lib.default, 
        "ReactRouterStrategy", 
        [],
        ["prepareRoutes", "prepareRoute", "preparePath", "prepareArguments", "setBrowserRouterSettings"],
        function(testClass, testInstance, classFunctions, instanceFunctions) {
            describe("The 'prepareRoutes' function should have the same signature", describeFunction(
                testInstance["prepareRoutes"], 
                ['routes']
            ));
            describe("The 'prepareRoute' function should have the same signature", describeFunction(
                testInstance["prepareRoute"], 
                ['route']
            ));
            describe("The 'preparePath' function should have the same signature", describeFunction(
                testInstance["preparePath"], 
                ['route']
            ));
            describe("The 'prepareArguments' function should have the same signature", describeFunction(
                testInstance["prepareArguments"], 
                ['params']
            ));
        }
    ));

    describe("Should return an object for controllers.", function(){
        it("Should return an object for controllers.", function(){
            assert.strictEqual(typeof lib.controllers, "object");
        });

        it("There should the same number of controllers types as the previous version", function(){
            assert.strictEqual(Object.keys(lib.controllers).length, 2);
        });
    });
    describe("Should return an object for components.", function(){
        it("Should return an object for components.", function(){
            assert.strictEqual(typeof lib.components, "object");
        });

        it("There should the same number of components types as the previous version", function(){
            assert.strictEqual(Object.keys(lib.components).length, 1);
        });
    });
});