import React from 'react';
import {ReactController} from '../core/controllers';

const withDependencyInjection = (Controller) => class DependencyInjectionAdapter {
    constructor(...args) {
        this.size = args.length;
        args.forEach((arg, i) => {
            this[i] = arg;
        });
    }

    getArguments() {
        let args = [];
        
        for(let i = 0; i < this.size; ++i) {
            args.push(this[i]);
        }

        return args;
    }

    getController() {
        let args = this.getArguments();
        return class DIControllerBridge extends ReactController {
            render() {
                return <Controller args={args} {...this.props} />
            }
        }
    }
};

export default withDependencyInjection;