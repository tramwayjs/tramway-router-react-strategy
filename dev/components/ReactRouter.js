import React, {Component} from 'react';
import {BrowserRouter} from 'react-router-dom'
import {Switch} from 'react-router'

export default class ReactRouter extends Component {
    /**
     * @param {{}} props
     */
    constructor(props) {
        super(props);
        const {notFoundRoute, routes, settings} = props;
        
        this.notFoundRoute = notFoundRoute;
        this.routes = routes;
        this.settings = settings;
    }

    /**
     * @return {Component}
     */
    render() {
        return (
            <BrowserRouter {...this.settings}>
                <Switch>
                    {this.routes}
                    {this.notFoundRoute}
                </Switch>
            </BrowserRouter>
        );
    }
}