import {Authentication, policies, entities} from 'tramway-core-router';
import React from 'react';
import {AuthorizationDecorator} from './router';

const {AuthenticationStrategy} = policies;
const {Route} = entities;

export default class Security {
    /**
     * Creates an instance of Security.
     * @param {AuthenticationStrategy} authenticationPolicy
     * @param {Route} route
     * @param {{}} props
     * @return {function(Object, Object, function(Object, Object))}
     * @memberOf Security
     */
    generateMiddleware(authenticationPolicy, route, props){
        let authentication = new Authentication(authenticationPolicy);
        return <AuthorizationDecorator Controller={route.getController()} policy={authentication} redirect={authenticationPolicy.getRedirectRoute()} {...props}/>
    }
}