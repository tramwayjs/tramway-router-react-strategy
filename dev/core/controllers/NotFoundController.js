import React from 'react';
import ReactController from './ReactController';
import {HttpStatus} from 'tramway-core-router';

export default class NotFoundController extends ReactController {
    /**
     * @static
     * @return {Number} status
     */
    static getStatus() {
        return HttpStatus.NOT_FOUND;
    }
}