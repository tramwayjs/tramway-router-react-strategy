import {Component} from 'react';
import {HttpStatus} from 'tramway-core-router';

export default class ReactController extends Component {
    /**
     * @param {{}} props
     */
    constructor(props) {
        super(props);

        const {params, location, history} = props;

        this.params = params;
        this.location = location;
        this.history = history;
    }

    /**
     * @static
     * @return {Number} status
     */
    static getStatus() {
        return HttpStatus.OK;
    }
}