import React, {Component} from 'react';
import {Redirect} from 'react-router'

export default class AuthorizationDecorator extends Component {  
    /**
     * @param {{}} props
     */  
    constructor(props) {
        super(props);

        const {Controller, match, policy, redirect} = props;

        this.Controller = Controller;
        this.params = match.params;
        this.policy = policy;
        this.redirect = redirect;

        this.state = {
            authorized: true
        };
    }

    async componentDidMount() {
        const {authorized} = this.params;

        if ("true" !== authorized) {
            return;
        }

        try {
            await this.policy.check();
            this.setState({authorized: true});
        } catch (e) {
            this.setState({authorized: false});
        }
    }

    /**
     * @return {Component}
     */
    render() {
        const {authorized} = this.state;
        const {Controller, params, props, redirect} = this;

        return authorized ? <Controller params={params} {...props} /> : <Redirect to={redirect}/>;
    }
}