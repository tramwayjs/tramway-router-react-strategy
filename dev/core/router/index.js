import AuthorizationDecorator from './AuthorizationDecorator';
import * as strategies from './strategies';

export {
    AuthorizationDecorator,
    strategies,
}