import {Router, Route as CoreRoute, HttpStatus} from 'tramway-core-router';
import React from 'react';
import {Route, withRouter} from 'react-router';
import Security from '../../Security';
import {ReactRouter} from '../../../components';
import { ReactController } from '../../controllers';

export default class ReactRouterStrategy {
    settings = {};

    /**
     * @param {Router} router
     * @param {Security} security
     */
    constructor(router, security) {
        this.router = router || ReactRouter;
        this.security = security || new Security();
    }

    /**
     * @param {[]} routes
     * @return {Component}
     */
    prepareRoutes(routes) {
        let reactRoutes = [];
        let notFoundRoute = null;

        for (let key in routes) {
            let route = routes[key];
            let Controller = route.getController();

            if (!(Controller.prototype instanceof ReactController)) {
                Controller = Controller.getController();
            }
            
            Controller = withRouter(Controller);

            if (HttpStatus.NOT_FOUND === Controller.getStatus()) {
                notFoundRoute = (
                    <Route 
                        key="notfound" 
                        render={props => <Controller params={props.match.params} {...props}/>}
                    />
                );
                continue;
            }

            reactRoutes.push(this.prepareRoute(route));
        }

        const ReactRouter = this.router;

        return <ReactRouter routes={reactRoutes} notFoundRoute={notFoundRoute} settings={this.settings}/>;
    }

    /**
     * @param {CoreRoute} route
     * @return {Route}
     */
    prepareRoute(route) {
        let path = this.preparePath(route);
        let Controller = route.getController();

        if (!(Controller.prototype instanceof ReactController)) {
            Controller = Controller.getController();
        }
        
        Controller = withRouter(Controller);

        const policy = route.getPolicy();

        if (policy) {
            return (
                <Route 
                    key={path} 
                    exact 
                    path={path} 
                    render={props => this.security.generateMiddleware(policy, route, props)}
                />
            );
        }

        return (
            <Route 
                key={path} 
                exact 
                path={path} 
                render={props => <Controller params={props.match.params} {...props} />}
            />
        );
    }

    /**
     * @param {CoreRoute} route
     * @returns {string}
     * 
     * @memberOf ExpressServerStrategy
     */
    preparePath(route) {
        let path = route.getPath();
        let params = this.prepareArguments(route.getArguments());

        if (path) {
            return Router.buildPath("/", path, params);
        } 

        return Router.buildPath("/", params);
        
    }

    /**
     * @param {string[] | string} params
     * @returns {string}
     * 
     * @memberOf ExpressServerStrategy
     */
    prepareArguments(params){
        if ("string" === typeof params) {
            return params;
        }

        let argString = params.join("/:");
        return `:${argString}`;
    }

    /**
     * @param {Object} settings
     * @property {string} settings.basename
     * @property {function} settings.getUserConfirmation
     * @property {boolean} settings.forceRefresh
     * @property {number} settings.keyLength
     */
    setBrowserRouterSettings(settings = {}) {
        this.settings = settings;
        return this;
    }
}