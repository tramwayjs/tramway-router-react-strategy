import {
    Security, 
    AuthorizationDecorator, 
    controllers, 
    strategies, 
} from './core';
import * as components from './components';
import {withDependencyInjection} from './adapters';

const {ReactRouterStrategy} = strategies;

export default ReactRouterStrategy;
export {
    AuthorizationDecorator, 
    Security, 
    controllers, 
    components, 
    withDependencyInjection,
};