Tramway Router React Strategy is a plugin for the Tramway Router allowing apps built with the framework to be served statically. It includes:

1. A dynamic routing system that separates routes from routing logic and is adaptable
2. Authentication policies that allow for multiple strategies to be used and interchanged without needing the logic throughout the code.
3. A simple but flexible React Router configuration

# Installation:
1. `npm install --save tramway-core-router tramway-router-react-strategy`

# Documentation

## Recommended Folder Structure
- components
- controllers
- policies
- routes
- styles
- public

## Components
All your basic React components that would be instantiated using the Controllers.

## Routes
Uses the same set up as a [traditional Tramway Router](https://gitlab.com/tramwayjs/tramway-core-router).

## Router
The Router will be called in your main index or bundle file where you instantiate the ReactDOM. This is typically at the root of your project. Once you have a router, initializing it will set up the routes and assign them to the Router which can then be assigned to the DOM.

Here's an example usage among parts of a main runner file:

```javascript
import {Router} from 'tramway-core-router';
import ReactRouterStrategy from 'tramway-router-react-strategy';

import routes from './routes/routes.js';
import './styles/index.css';

let strategy = new ReactRouterStrategy();
let router = new Router(routes, strategy).initialize();

ReactDOM.render(router, document.getElementById('root'));
```

> The same setup can be achieved with `tramway-core-react-dependency-injector` as follows:
> Add a `routers.js` file under your services config.
> ```javascript
> import {Router} from 'tramway-core-router';
> import {DependencyResolver} from 'tramway-core-react-dependency-injector';
> import ReactRouterStrategy from 'tramway-router-react-strategy';
>
> export default {
>   "router": {
>        "class": Router,
>        "constructor": [
>            {"type": "parameter", "key": "routes"},
>            {"type": "service", "key": "react-router-strategy"},
>            DependencyResolver,
>        ],
>    },
>    "react-router-strategy": {
>        "class": ReactRouterStrategy,
>        "constructor": [],
>        "functions": [
>            {
>                "function": "setBrowserRouterSettings", 
>                "args": [{"type": "parameter", "key": "routerOptions"}]
>            }
>        ]
>    }
> }
> ```
> The library includes an adapter to allow `ReactController` to be instantiated from within the container as well. In your `controllers.js` file under your services config, import and wrap each controller with the adapter.
> ```javascript
> import {
>    MainController,
>    PageNotFoundController,
> } from '../../controllers';
>
> import {withDependencyInjection} from 'tramway-router-react-strategy';
>
> export default {
>    "controller.main": {
>        "class": withDependencyInjection(MainController),
>        "constructor": [
>            {"type": "service", "key": "main.service"},
>        ],
>    },
>    "controller.not_found": {
>        "class": withDependencyInjection(PageNotFoundController),
>    },
> }
> ```
> In your controller, you can access the args as an ordered array under the `args` prop.
> ```javascript
> export default class MainController extends ReactController {
>    constructor({args: [mainService], ...props}) {
>        super(props);
>        this.mainService = mainService;
>    }
> }
> ```

Note that this setup will require an index.html file to be present with an element that has the id "root" in order for this to work. This setup may vary based on whether you use gulp or webpack to handle the bundling.

The `ReactRouter` that's provided with this library creates a new `BrowserRouter` as per React-Router's V4 API and uses a switch between all the routes in the config and a separated 404 handling which can be done by using the included `NotFoundController` with one of the routes in the routing configuration. If you wish to override the `ReactRouter` with your own Router setup - a `Switch` is not ideal for parallax websites - make sure that the following parameters are given to the `ReactRouter` component you pass to the `RouterStrategy`:

| Param | Type | Purpose |
| --- | --- | --- |
| notFoundRoute | `React.Component` | The React Router strategy will identify the NotFoundRoute response for any unspecified url |
| routes | `React.Component[]` | The React Router strategy will convert the routes config as per `tramway-core-router@3^` to the React-Router V4 spec Routes and return them in an array to be nested as per the React API |

The `ReactRouterStrategy` allows for various configuration adjustments. Notably, the React Router and Security management can be adjusted by passing constructor arguments, and the settings of the React Router itself can be adjusted by passing a configuration object to the `setBrowserRouterSettings()` method.

```javascript
strategy.setBrowserRouterSettings({
  basename: string,
  getUserConfirmation: function,
  forceRefresh: boolean,
  keyLength: number,
})
```

## Controllers
Controllers link to actions from the routing and act to direct the flow of the application.

Controllers with the React Router are a bit different than with the traditional routing in Tramway and traditional Tramway Controllers will not work here.

To create a controller, import the class and implement the render function and lifecycle methods for any api calls.

```javascript
import {controllers} from 'tramway-router-react-strategy';
const {ReactController} = controllers;
```

To create a 404 Not Found Page, simply extend and implement the `NotFoundController`.

```javascript
import {controllers} from 'tramway-router-react-strategy';
const {NotFoundController} = controllers;
```

Accessing route parameters is encapsulated using `ReactController` and its children.

| Param | Type | Purpose |
| --- | --- | --- |
| params | `{}` | Contains the parameters passed through the router. If you have a path with argument: `name`, expect params to have ```{"name": "NAME_FROM_URL"}``` |
| location | `{}` | Contains the pathname, search and state that was passed to the Router |
| history | `{}` | Contains all the actions to manipulate the browser history within the Router |

All `ReactController` classes will be wrapped by the `withRouter` decorator when the route is built by the `ReactRouterStrategy`.

## Policies
The policies architecture is identical to that of any [standard Tramway application](https://gitlab.com/tramwayjs/tramway-core-router). Note that any controller containing a policy will be wrapped in an `AuthorizationDecorator` by this library's version of the `Security` class and will asynchronously determine if the application should redirect to the policy's redirect path.